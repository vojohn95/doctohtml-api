import mammoth
from fastapi import FastAPI, File, UploadFile
from fastapi.responses import FileResponse
from docx2pdf import convert
from pdf2docx import Converter
import sys
import os, os.path

# import comtypes.client
# from docx.api import Document
# import pandas as pd

app = FastAPI()


@app.get("/")
def home():
    return {"Mensaje": "DocToHtml"}


@app.get("/convhtml/{base64string}")
async def read_item(base64string):
    return {"stringhtml": base64string}


# docx to html
@app.post("/docxtohtml/")
async def upload_docx(file: UploadFile = File(...)):
    if not file:
        return {"message": "No upload file sent"}
    else:
        try:
            contents = file.file.read()
            with open(file.filename, 'wb') as f:
                f.write(contents)
            # start convertion
            with open(file.filename, "rb") as docx_file:
                result = mammoth.convert_to_html(docx_file)
        except Exception as e:
            os.remove(file.filename)
            return {"error": e}
        finally:
            file.file.close()
            os.remove(file.filename)

        return {"message": result}


# docx to pdf
@app.post("/docxtopdf/")
async def upload_docx(file: UploadFile = File(...)):
    if not file:
        return {"message": "No upload file sent"}
    else:
        try:
            contents = file.file.read()
            with open(file.filename, 'wb') as f:
                f.write(contents)
            # start convertion
            with open(file.filename, "rb") as docx_file:
                result = convert(file.filename)
        except Exception as e:
            os.remove(file.filename)
            return {"error": e}
        finally:
            file.file.close()
            os.remove(file.filename)
            return FileResponse(result)

        return {"message": result}

# pdf to docx
@app.post("/pdftodocx/")
async def upload_docx(file: UploadFile = File(...)):
    if not file:
        return {"message": "No upload file sent"}
    else:
        try:
            contents = file.file.read()
            with open(file.filename, 'wb') as f:
                f.write(contents)
            # start convertion
            with open(file.filename, "rb") as docx_file:
                result = Converter(file.filename)
        except Exception as e:
            os.remove(file.filename)
            return {"error": e}
        finally:
            file.file.close()
            os.remove(file.filename)

        return {"message": result}

# docx to xls
# document = Document("AKSMS.docx")
# tables = document.tables
# df = pd.DataFrame()

# for table in document.tables:
#    for row in table.rows:
#        text = [cell.text for cell in row.cells]
#        df = df.append([text], ignore_index=True)

# df.columns = ["Column1", "Column2"]
# df.to_excel("test.xlsx")
# print df

# rtf to pdf
# wdFormatPDF = 17

# input_dir = '/'
# output_dir = '/'

# for subdir, dirs, files in os.walk(input_dir):
#     for file in files:
#         in_file = os.path.join(subdir, file)
#         output_file = file.split('.')[0]
#         out_file = output_dir+output_file+'.pdf'
#         word = comtypes.client.CreateObject('Word.Application')

#         doc = word.Documents.Open(in_file)
#         doc.SaveAs(out_file, FileFormat=wdFormatPDF)
#         doc.Close()
#         word.Quit()
